package com.cgi.registration_login.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cgi.registration_login.domain.Domain;
import com.cgi.registration_login.service.ServiceI;

@RestController
@RequestMapping("api/v1")
public class Controller {

	private ServiceI service;
	

	@Autowired
	public Controller(ServiceI service) {
		super();
		this.service = service;
	}


	@PostMapping("register")
	public ResponseEntity<?> registerUser(@RequestBody Domain domain){
		return new ResponseEntity<Domain>(service.saveUser(domain),HttpStatus.CREATED);
	}
	
	@GetMapping("test/{email}")
	public ResponseEntity<?> test(@PathVariable String email){
		return new ResponseEntity<Domain>(service.test(email),HttpStatus.OK);
	}
	
	@PostMapping("login")
	public ResponseEntity<?> loginUser(@RequestBody Domain domain) throws Exception{
		return new ResponseEntity<Map<String,String>>(service.login(domain),HttpStatus.OK);
	}
	
}
