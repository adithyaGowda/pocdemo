package com.cgi.registration_login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cgi.registration_login.domain.Domain;

@Repository
public interface Repo extends JpaRepository<Domain, String> {

	Domain findByEmail(String email);
}
