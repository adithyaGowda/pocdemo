package com.cgi.registration_login.jwt;

import java.util.Map;

import com.cgi.registration_login.domain.Domain;

@FunctionalInterface
public interface SecurityTokenGenrator {

	Map<String, String> generateToken(Domain doamin);
}
