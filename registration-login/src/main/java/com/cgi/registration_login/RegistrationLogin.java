package com.cgi.registration_login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class RegistrationLogin
{
    public static void main( String[] args )
    {

        SpringApplication.run(RegistrationLogin.class, args);
    }
}
