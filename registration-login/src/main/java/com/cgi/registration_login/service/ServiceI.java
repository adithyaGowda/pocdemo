package com.cgi.registration_login.service;

import java.util.Map;
import com.cgi.registration_login.domain.Domain;


public interface ServiceI {

	Domain saveUser(Domain domain);

	Domain test(String email);

	Map<String, String> login(Domain domain) throws Exception;

}
