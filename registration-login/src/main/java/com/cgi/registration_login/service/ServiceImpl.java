package com.cgi.registration_login.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import com.cgi.registration_login.domain.Domain;
import com.cgi.registration_login.jwt.SecurityTokenGenrator;
import com.cgi.registration_login.repository.Repo;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class ServiceImpl implements ServiceI {

	private Repo repo;
	
	@Autowired
	public ServiceImpl(Repo repo) {
		this.repo = repo;
	}

	public Domain saveUser(Domain domain) {
		return repo.save(domain);
	}

	public Domain test(String email) {
		return repo.findByEmail(email);
	}

	public Map<String, String> login(Domain domain) throws Exception {
		
		String email = domain.getEmail();
		String pwd = domain.getPassword();
		
		Domain user = repo.findByEmail(email);
		
		if(user == null) {
			throw new Exception("Not found");
		}
		
		System.out.println(email);
		System.out.println(pwd);
		System.out.println(user.toString());
		
		
		if (!email.equals(user.getEmail()) || !pwd.equals(user.getPassword())) {
			
			throw new Exception("Invalid credentials");
		}
		
		SecurityTokenGenrator genrator = (Domain userDetails) -> {
			String jwtToken = "";

            jwtToken = Jwts.builder().setId(""+user.getEmail()).setIssuedAt(new Date()).setSubject("User")
                    .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

            Map<String, String> map1 = new HashMap<>();

            map1.put("token", jwtToken);

            map1.put("message", "User successfully logged in");

            return map1;
		};
	
		Map<String, String> map = genrator.generateToken(user);
		
		return map;
	}


	
}
