package com.pocDemo.service;

import java.util.List;
import java.util.Optional;

import com.pocDemo.domain.Example;
import com.pocDemo.exceptions.MemberAlreadyFoundException;
import com.pocDemo.exceptions.MemberNotFoundException;

public interface ExampleProjectService {

	Example saveMember(Example example) throws MemberAlreadyFoundException;

	Example updateMember(Example example) throws MemberNotFoundException;

	List<Example> getAllMembers() throws Exception;

	Optional<Example> deleteMember(Example example) throws MemberNotFoundException;

	Example getMemberById(int id) throws MemberNotFoundException;

	Optional<Example> deleteMemberById(int id) throws MemberNotFoundException;


}
