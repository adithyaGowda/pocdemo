package com.pocDemo.repository;

import com.pocDemo.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExampleProjectRepository extends JpaRepository<Example, Integer> {

}
