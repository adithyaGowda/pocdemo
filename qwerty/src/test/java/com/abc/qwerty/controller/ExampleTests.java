package com.abc.qwerty.controller;

import com.abc.qwerty.domain.Example;
import com.abc.qwerty.exception.AlreadyFoundException;
import com.abc.qwerty.service.ExampleServ;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ExampleTests {

    private Example example;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExampleServ exampleServ;
    private List<Example> list;

    @InjectMocks
    private ExampleController exampleController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(exampleController).build();

        example = new Example();
        example.setId(100);
        example.setName("peter");

        list = new ArrayList<>();
        list.add(example);
    }
    @After
    public void tearDown(){
        this.example = null;
        this.list = null;
    }

    @Test
    public void inputDataShouldSavedInDatabaseAndReturnSavedData() throws Exception {

        when(exampleServ.saveData(any())).thenReturn(example);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/post")
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(example)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andDo(MockMvcResultHandlers.print());

        verify(exampleServ, times(1)).saveData(example);
    }

//    @Test
//    public void inputDataShouldReturnException() throws Exception {
//
//        when(exampleServ.saveData(any())).thenThrow(AlreadyFoundException.class);
//
//        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/post")
//                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(example)))
//                .andExpect(MockMvcResultMatchers.status().isConflict())
//                .andDo(MockMvcResultHandlers.print());
//
//        verify(exampleServ, times(1)).saveData(example);
//    }

    @Test
    public void urlShouldReturnTheListOfData() throws Exception {

        when(exampleServ.getData()).thenReturn(list);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/get")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

        verify(exampleServ, times(1)).getData();
    }

    private static String asJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
