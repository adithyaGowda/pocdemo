package com.abc.qwerty.controller;

import com.abc.qwerty.domain.Example;
import com.abc.qwerty.exception.AlreadyFoundException;
import com.abc.qwerty.exception.NotFoundException;
import com.abc.qwerty.service.ExampleServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class ExampleController {
    private ExampleServ exampleServ;

    @Autowired
    public ExampleController(ExampleServ exampleServ) {
        this.exampleServ = exampleServ;
    }

    @PostMapping("post")
    public ResponseEntity<?> saveData(@RequestBody Example example) throws AlreadyFoundException {
        return new ResponseEntity<Example>(exampleServ.saveData(example), HttpStatus.CREATED);
    }

    @GetMapping("get")
    public ResponseEntity<?> getData() throws Exception {
        return  new ResponseEntity<List<Example>>(exampleServ.getData(),HttpStatus.OK);
    }

    @GetMapping("get/{id}")
    public ResponseEntity<?> getDataById(@PathVariable int id) throws NotFoundException {
        return new ResponseEntity<Example>(exampleServ.getById(id),HttpStatus.OK);
    }

//    @GetMapping("get/{name}")
//    public ResponseEntity<?> getDataById(@PathVariable String name) throws NotFoundException {
//        return new ResponseEntity<Example>(exampleServ.getByName(name),HttpStatus.OK);
//    }

}
