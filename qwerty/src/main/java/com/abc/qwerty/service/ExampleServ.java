package com.abc.qwerty.service;

import com.abc.qwerty.domain.Example;
import com.abc.qwerty.exception.AlreadyFoundException;
import com.abc.qwerty.exception.NotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public interface ExampleServ {
    Example saveData(Example example) throws AlreadyFoundException;

    List<Example> getData() throws Exception;

    Example getById(int id) throws NotFoundException;

//    Example getByName(EntityManager em,String name) throws NotFoundException;
}
