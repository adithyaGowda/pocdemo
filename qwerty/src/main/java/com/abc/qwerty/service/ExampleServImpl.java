package com.abc.qwerty.service;

import com.abc.qwerty.domain.Example;
import com.abc.qwerty.exception.AlreadyFoundException;
import com.abc.qwerty.exception.NotFoundException;
import com.abc.qwerty.repository.ExampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class ExampleServImpl implements ExampleServ {

    private ExampleRepository exampleRepository;

    @Autowired
    public ExampleServImpl(ExampleRepository exampleRepository) {
        this.exampleRepository = exampleRepository;
    }

    @Override
    public Example saveData(Example example) throws AlreadyFoundException {
        return exampleRepository.save(example);
    }

    @Override
    public List<Example> getData() throws Exception {
        return exampleRepository.findAll();
    }

    @Override
    public Example getById(int id) throws NotFoundException {
        if (!exampleRepository.existsById(id)){
            throw new NotFoundException("NOT FOUND");
        }

        return null;
    }


    public Example getByName(EntityManager em, String name) {
        TypedQuery<Example> query = em.createQuery(
                "SELECT c FROM Example c WHERE c.name=:name",Example.class
        );
        return query.setParameter("name",name).getSingleResult();
    }
}
