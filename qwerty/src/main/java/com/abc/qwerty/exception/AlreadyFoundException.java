package com.abc.qwerty.exception;

public class AlreadyFoundException extends Exception {
    private String message;

    public AlreadyFoundException(String message) {
        this.message = message;
    }
}
