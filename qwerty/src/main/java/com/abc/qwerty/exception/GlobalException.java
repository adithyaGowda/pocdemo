package com.abc.qwerty.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = AlreadyFoundException.class)
    public ResponseEntity<Object> alreadyFound(AlreadyFoundException e) {
        return new ResponseEntity<>("Already found", HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<Object> notFound(NotFoundException exp) {
        return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
    }

}
